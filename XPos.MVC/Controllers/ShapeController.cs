﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace XPos.MVC.Controllers
{
    public class ShapeController : Controller
    {
        // GET: Shape
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Green(long val)
        {
            ViewBag.Value = val;
            return PartialView("_Green");
        }


        public ActionResult Yellow(long val)
        {
            ViewBag.Value = val;
            return PartialView("_Yellow");
        }
    }
}