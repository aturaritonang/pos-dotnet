﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.DataAccess;
using XPos.ViewModel;

namespace XPos.MVC.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page = 1, int cnt = 10)
        {
            var ListProduct = ProductRepo.All(page, cnt);
            decimal pgTotal = (decimal)ListProduct.Item2 / cnt;
            int fullPage = ListProduct.Item2 / cnt;
            if (pgTotal - fullPage > 0)
            {
                fullPage += 1;
            }
            ViewBag.PageTotal = fullPage;
            return PartialView("_List", ListProduct.Item1);
        }

        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(0), "Id", "Name");
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            HttpPostedFileBase file = model.File;
            string[] fileName = file.FileName.Split('.');
            string newFile = DateTime.Now.ToString("yyyyMMddHHmmss") + "." + fileName[fileName.Length - 1];
            model.Image = newFile;
            ResponseResult result = ProductRepo.Update(model);
            if (result.Success)
            {
                if (file != null && file.ContentLength > 0)
                    try
                    {
                        string path = Path.Combine(Server.MapPath("~/FileUpload"),
                                                   Path.GetFileName(newFile));
                        file.SaveAs(path);
                    }
                    catch (Exception ex)
                    {
                        result.Success = false;
                        result.Message = ex.Message;
                    }
                else
                {
                    result.Success = false;
                    result.Message = "You have not specified a file.";
                }
            }
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(long id)
        {
            ProductViewModel model = ProductRepo.ById(id);
            string fileName = "/FileUpload/no-image-icon-11.png";
            if (!String.IsNullOrEmpty(model.Image))
            {
                fileName = "/FileUpload/" + model.Image;
            }
            ViewBag.FileName = fileName;
            ViewBag.CategoryList = new SelectList(CategoryRepo.All(), "Id", "Name");
            ViewBag.VariantList = new SelectList(VariantRepo.ByCategory(model.CategoryId), "Id", "Name");
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            HttpPostedFileBase file = model.File;
            string[] fileName = file.FileName.Split('.');
            string newFile = DateTime.Now.ToString("yyyyMMddHHmmss") + "." + fileName[fileName.Length - 1];
            model.Image = newFile;
            ResponseResult result = ProductRepo.Update(model);
            if (result.Success)
            {
                if (file != null && file.ContentLength > 0)
                {
                    try
                    {
                        string path = Path.Combine(Server.MapPath("~/FileUpload"), Path.GetFileName(newFile));
                        file.SaveAs(path);
                    }
                    catch (Exception ex)
                    {
                        result.Success = false;
                        result.Message = ex.Message;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = "You have not specified a file.";
                }
            }
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ProductList(string search = "")
        {
            var result = ProductRepo.ByFilter(search);
            return PartialView("_ProductList", result.Item1);
        }
    }
}