﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace XPos.MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult UploadFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            if (file != null && file.ContentLength > 0)
                try
                {
                    string[] fileName = file.FileName.Split('.');
                    string newFile = DateTime.Now.ToString("yyyyMMddHHmmss") + "." 
                        + fileName[fileName.Length - 1];
                    string path = Path.Combine(Server.MapPath("~/FileUpload"),
                                               Path.GetFileName(newFile));
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                    ViewBag.UploadedFile = "\\FileUpload\\" + newFile;
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return View();
        }
    }
}